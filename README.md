# merge-simulator

Alla ändringar görs via branches och mergas in via en merge request. Person 1 fortsätter att lägga till ingredienser eftersom, så att de andra måste anpassa sig.

- Person 1 ansvarar för att lägga till ingredienser i en lista
- Person 2 ska skriva beskrivning hur receptet utförs
- Person 3 ansvarar för hur mycket av varje som ska användas, både i listan och i den beskrivande texten.

## Ingredienser

- sallad
- 1 l havremjölk
- kokosdryck
- bröd
- 1 kg ost
- 2 tablespoons sherry
- ½ teaspoon ground black pepper
- 3 cup coarsely chopped fresh mushrooms
- 0.5 kg köttfärs
- 1 cup coarsely chopped fresh mushrooms
- tomatsås
- pasta


## Directions

1. väg upp sallad och skölj.
2. tillsätt havremjölk.
3. koka pasta.
4. Cook, stirring constantly, until flavors are well incorporated, about 10 minutes.
5. riv ost och sprid över havremjölken.
6. rosta skivor bröd
7. lägg på allt på brödet.
8. släng köttfärsen.
9. drick upp Sherryn.


